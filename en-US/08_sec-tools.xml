<?xml version="1.0"?>
<chapter id="sec-tools"><title>Security tools in Debian</title>

<para>FIXME: More content needed.

</para><para>Debian provides also a number of security tools that can make a
Debian box suited for security purposes. These purposes include
protection of information systems through firewalls (either packet or
application-level), intrusion detection (both network and host based),
vulnerability assessment, antivirus, private networks, etc.

</para><para>Since Debian 3.0 (<emphasis>woody</emphasis>), the distribution features
cryptographic software integrated into the main distribution. OpenSSH
and GNU Privacy Guard are included in the default install, and strong
encryption is now present in web browsers and web servers, databases,
and so forth. Further integration of cryptography is planned for
future releases. This software, due to export restrictions in the US,
was not distributed along with the main distribution but included only
in non-US sites.</para>

<section id="vuln-asses"><title>Remote vulnerability assessment tools</title>

<para>The tools provided by Debian to perform remote vulnerability assessment
are:
<footnote><para>
Some of them are provided
when installing the <application>harden-remoteaudit</application> package.
</para></footnote>
<itemizedlist>
    <listitem><para><application>nessus</application></para></listitem>
<listitem><para><application>raccess</application></para></listitem>
<listitem><para><application>nikto</application> (<command>whisker</command>'s replacement)</para></listitem>
</itemizedlist>

</para><para>By far, the most complete and up-to-date tools is
<application>nessus</application> which is composed of a client
(<application>nessus</application>) used as a GUI and a server
(<application>nessusd</application>) which launches the programmed
attacks. Nessus includes remote vulnerabilities for quite a number of
systems including network appliances, ftp servers, www servers,
etc. The latest security plugins are able even to parse a web site and
try to discover which interactive pages are available which could be
attacked. There are also Java and Win32 clients (not included in
Debian) which can be used to contact the management server. 

</para><para><application>nikto</application> is a web-only vulnerability assessment scanner
including anti-IDS tactics (most of which are not <emphasis>anti-IDS</emphasis> anymore).
It is one of the best cgi-scanners available, being able to detect 
a WWW server and launch only a given set of attacks against it. The database
used for scanning can be easily modified to provide for new information.</para>
</section>

<section><title>Network scanner tools</title>
<para>Debian does provide some tools used for remote scanning of hosts 
(but not vulnerability assessment). These tools are, in some cases,
used by vulnerability assessment scanners as the first type of 
"attack" run against remote hosts in an attempt to 
determine remote services available. Currently Debian provides:
<itemizedlist>
<listitem><para><application>nmap</application></para></listitem>
<listitem><para><application>xprobe</application></para></listitem>
<listitem><para><application>p0f</application></para></listitem>
<listitem><para><application>knocker</application></para></listitem>
<listitem><para><application>isic</application></para></listitem>
<listitem><para><application>hping2</application></para></listitem>
<listitem><para><application>icmpush</application></para></listitem>
<listitem><para><application>nbtscan</application> (for SMB /NetBIOS audits)</para></listitem>
<listitem><para><application>fragrouter</application></para></listitem>
<listitem><para><command>strobe</command> (in the <application>netdiag</application> package)</para></listitem>
<listitem><para><application>irpas</application></para></listitem>
</itemizedlist>

<!--
Ettercap is not included since its a sniffing tool not a remote probe.
-->

</para><para>While <application>xprobe</application> provide
only remote operating system detection (using TCP/IP fingerprinting,
<application>nmap</application> and <application>knocker</application> do both operating
system detection and port scanning of the remote hosts. On the other
hand, <application>hping2</application> and <application>icmpush</application> can be
used for remote ICMP attack techniques.

</para><para>Designed specifically for SMB networks, <application>nbtscan</application>
can be used to scan IP networks and retrieve name information from 
SMB-enabled servers, including: usernames, network names, MAC
addresses...

</para><para>On the other hand, <application>fragrouter</application> can be used to
test network intrusion detection systems and see if the NIDS can be
eluded by fragmentation attacks.

</para><para>FIXME: Check <ulink url="http://bugs.debian.org/153117" name="Bug
#153117" /> (ITP fragrouter) to see if it's included.

</para><para>FIXME add information based on
<ulink url="http://www.giac.org/practical/gcux/Stephanie_Thomas_GCUX.pdf"
name="Debian Linux Laptop for Road Warriors" /> which describes how to
use Debian and a laptop to scan for wireless (803.1) networks (link not
there any more).</para>
</section>

<section><title>Internal audits</title>
<para>Currently, only the <application>tiger</application> tool used in Debian can
be used to perform internal (also called white box) audit of hosts in
order to determine if the file system is properly set up, which processes
are listening on the host, etc.</para>
</section>

<section><title>Auditing source code</title>
<para>Debian provides several packages that can be used to audit C/C++ source code
programs and find programming errors that might lead to potential security
flaws:
<itemizedlist>
<listitem><para><application>flawfinder</application></para></listitem>
<listitem><para><application>rats</application></para></listitem>
<listitem><para><application>splint</application></para></listitem>
<listitem><para><application>pscan</application></para></listitem>
</itemizedlist></para>
</section>

<section id="vpn"><title>Virtual Private Networks</title>

<para>A virtual private network (VPN) is a group of two or more computer
systems, typically connected to a private network with limited public
network access, that communicate securely over a public network. VPNs
may connect a single computer to a private network (client-server), or
a remote LAN to a private network (server-server). VPNs often include
the use of encryption, strong authentication of remote users or hosts,
and methods for hiding the private network's topology.

</para><para>Debian provides quite a few packages to set up encrypted virtual
private networks:

<itemizedlist>

<listitem><para><application>vtun</application></para></listitem>
<listitem><para><application>tunnelv</application> (non-US section)</para></listitem>
<listitem><para><application>cipe-source</application>, <application>cipe-common</application></para></listitem>
<listitem><para><application>tinc</application></para></listitem>
<listitem><para><application>secvpn</application></para></listitem>
<listitem><para><application>pptpd</application></para></listitem>
<listitem><para><application>openvpn</application></para></listitem>
<listitem><para><application>openswan</application> (<ulink url="http://www.openswan.org/" />)</para></listitem>

</itemizedlist>

</para><para>FIXME: Update the information here since it was written with
FreeSWAN in mind. Check Bug #237764 and Message-Id: 
&lt;200412101215.04040.rmayr@debian.org&gt;.


</para><para>The OpenSWAN package is probably the best choice overall, since it
promises to interoperate with almost anything that uses the IP
security protocol, IPsec (RFC 2411). However, the other packages
listed above can also help you get a secure tunnel up in a hurry. The
point to point tunneling protocol (PPTP) is a proprietary Microsoft
protocol for VPN. It is supported under Linux, but is known to have
serious security issues.

</para><para>For more information see the <ulink
url="http://www.tldp.org/HOWTO/VPN-Masquerade-HOWTO.html"
name="VPN-Masquerade HOWTO" /> (covers IPsec and PPTP), <ulink
url="http://www.tldp.org/HOWTO/VPN-HOWTO.html" name="VPN HOWTO" />
(covers PPP over SSH), <ulink
url="http://www.tldp.org/HOWTO/mini/Cipe+Masq.html" name="Cipe
mini-HOWTO" />, and <ulink
url="http://www.tldp.org/HOWTO/mini/ppp-ssh/index.html" name="PPP
and SSH mini-HOWTO" />.

</para><para>Also worth checking out is
<ulink url="http://yavipin.sourceforge.net/" name="Yavipin" />, but no Debian
packages seem to be available yet.</para>

<section><title>Point to Point tunneling</title>

<para>If you want to provide a tunneling server for a mixed environment
(both Microsoft operating systems and Linux clients) and IPsec is not
an option (since it's only provided for Windows 2000 and Windows XP),
you can use <emphasis>PoPToP</emphasis> (Point to Point Tunneling Server),
provided in the <application>pptpd</application> package.

</para><para>If you want to use Microsoft's authentication and encryption with
the server provided in the <application>ppp</application> package, note the
following from the FAQ:

<screen>
It is only necessary to use PPP 2.3.8 if you want Microsoft compatible
MSCHAPv2/MPPE authentication and encryption. The reason for this is that
the MSCHAPv2/MPPE patch currently supplied (19990813) is against PPP
2.3.8. If you don't need Microsoft compatible authentication/encryption
any 2.3.x PPP source will be fine.
</screen>

</para><para>However, you also have to apply the kernel patch provided by the
<application>kernel-patch-mppe</application> package, which provides the
pp_mppe module for pppd.

</para><para>Take into account that the encryption in ppptp forces you to store
user passwords in clear text, and that the MS-CHAPv2 protocol contains
<ulink url="http://mopo.informatik.uni-freiburg.de/pptp_mschapv2/"
name="known security holes" />.</para>
</section></section>

<section><title>Public Key Infrastructure (PKI)</title>

<para>Public Key Infrastructure (PKI) is a security architecture
introduced to provide an increased level of confidence for exchanging
information over insecure networks. It makes use of the concept of
public and private cryptographic keys to verify the identity of the
sender (signing) and to ensure privacy (encryption).

</para><para>When considering a PKI, you are confronted with a wide variety of
issues:

<itemizedlist>

<listitem><para>a Certificate Authority (CA) that can issue and verify
certificates, and that can work under a given hierarchy.</para></listitem>

<listitem><para>a Directory to hold user's public certificates.</para></listitem>

<listitem><para>a Database (?) to maintain Certificate Revocation Lists (CRL).</para></listitem>

<listitem><para>devices that interoperate with the CA in order to print out
smart cards/USB tokens/whatever to securely store certificates.</para></listitem>

<listitem><para>certificate-aware applications that can use certificates issued
by a CA to enroll in encrypted communication and check given
certificates against CRL (for authentication and full Single Sign On
solutions).</para></listitem>

<listitem><para>a Time stamping authority to digitally sign documents.</para></listitem>

<listitem><para>a management console from which all of this can be properly used
(certificate generation, revocation list control, etc...).</para></listitem>

</itemizedlist>

</para><para> Debian GNU/Linux has software packages to help you with some of
these PKI issues. They include <command>OpenSSL</command> (for certificate
generation), <command>OpenLDAP</command> (as a directory to hold the
certificates), <command>gnupg</command> and <command>openswan</command> (with
X.509 standard support). However, as of the Woody release (Debian
3.0), Debian does not have any of the freely available Certificate
Authorities such as pyCA, <ulink url="http://www.openca.org"
name="OpenCA" /> or the CA samples from OpenSSL. For more information
read the <ulink url="http://ospkibook.sourceforge.net/" name="Open PKI
book" />.</para>
</section>

<section><title>SSL Infrastructure</title>

<para>Debian does provide some SSL certificates with the distribution so
that they can be installed locally. They are found in the
<application>ca-certificates</application> package. This package provides a
central repository of certificates that have been submitted to Debian
and approved (that is, verified) by the package maintainer, useful for
any OpenSSL applications which verify SSL connections.

</para><para>FIXME: read debian-devel to see if there was something added to this.</para>
</section>

<section><title>Antivirus tools</title>

<para>There are not many anti-virus tools included with Debian GNU/Linux,
probably because GNU/Linux users are not plagued by viruses. The Unix
security model makes a distinction between privileged (root) processes
and user-owned processes, therefore a "hostile" executable that a
non-root user receives or creates and then executes cannot "infect" or
otherwise manipulate the whole system. However, GNU/Linux worms and
viruses do exist, although there has not (yet, hopefully) been any
that has spread in the wild over any Debian distribution. In any case,
administrators might want to build up anti-virus gateways that protect
against viruses arising on other, more vulnerable systems in their
network.

</para><para>Debian GNU/Linux currently provides the following tools for
building antivirus environments:

<itemizedlist>

<listitem><para><ulink url="http://www.clamav.net" name="Clam Antivirus" />,
provided since Debian <emphasis>sarge</emphasis> (3.1 release). Packages are
provided both for the virus scanner (<application>clamav</application>) for
the scanner daemon (<application>clamav-daemon</application>) and for the data
files needed for the scanner. Since keeping an antivirus up-to-date is
critical for it to work properly there are two different ways to get
this data: <application>clamav-freshclam</application> provides a way to
update the database through the Internet automatically and
<application>clamav-data</application> which provides the data files directly.
<footnote><para>If you use this last package and are running an official
Debian, the database will not be updated with security updates. You
should either use <application>clamav-freshclam</application>,
<command>clamav-getfiles</command> to generate new
<application>clamav-data</application> packages or update from the
maintainers location:
<screen>
  deb http://people.debian.org/~zugschlus/clamav-data/ /
  deb-src http://people.debian.org/~zugschlus/clamav-data/ /
</screen>
</para></footnote></para></listitem>

<listitem><para><application>mailscanner</application> an e-mail gateway virus scanner
and spam detector. Using <application>sendmail</application> or
<application>exim</application> as its basis, it can use more than 17
different virus scanning engines (including <application>clamav</application>).</para></listitem>

<listitem><para><application>libfile-scan-perl</application> which provides File::Scan,
a Perl extension for scanning files for viruses. This modules can be
used to make platform independent virus scanners.</para></listitem>

<listitem><para><ulink url="http://www.sourceforge.net/projects/amavis"
name="Amavis Next Generation" />, provided in the package
<application>amavis-ng</application> and available in <emphasis>sarge</emphasis>, which is
a mail virus scanner which integrates with different MTA (Exim,
Sendmail, Postfix, or Qmail) and supports over 15 virus scanning
engines (including clamav, File::Scan and openantivirus).</para></listitem>

<listitem><para><ulink url="http://packages.debian.org/sanitizer"
name="sanitizer" />, a tool that uses the <application>procmail</application>
package, which can scan email attachments for viruses, block
attachments based on their filenames, and more.</para></listitem>

<listitem><para><ulink url="http://packages.debian.org/amavis-postfix"
name="amavis-postfix" />, a script that provides an interface from a
mail transport agent to one or more commercial virus scanners (this
package is built with support for the <command>postfix</command> MTA only).</para></listitem>

<listitem><para><application>exiscan</application>, an e-mail virus scanner written in
Perl that works with Exim.</para></listitem>

<listitem><para><application>blackhole-qmail</application> a spam filter for Qmail with
built-in support for Clamav.</para></listitem>
</itemizedlist>

</para><para>Some gateway daemons support already tools extensions to build
antivirus environments including <application>exim4-daemon-heavy</application>
(the <emphasis>heavy</emphasis> version of the Exim MTA), <application>frox</application>
(a transparent caching ftp proxy server),
<application>messagewall</application> (an SMTP proxy daemon) and
<application>pop3vscan</application> (a transparent POP3 proxy).

</para><para>Debian currently provide <command>clamav</command> as the only antivirus scanning
software in the main official distribution and it also provides multiple
interfaces to build gateways with antivirus capabilities for different
protocols.

</para><para>Some other free software antivirus projects which might be included
in future Debian GNU/Linux releases:<ulink url="http://sourceforge.net/projects/openantivirus/" name="Open
Antivirus" /> (see 
<ulink
url="http://bugs.debian.org/150698" name="Bug #150698 (ITP oav-scannerdaemon)" /> 
and <ulink url="http://bugs.debian.org/150695" name="Bug #150695 (ITP oav-update)" />
).

</para><para>FIXME: Is there a package that provides a script to download the latest
virus signatures from <ulink url="http://www.openantivirus.org/latest.php" />?

</para><para>FIXME: Check if scannerdaemon is the same as the open antivirus scanner 
daemon (read ITPs).

</para><para>However, Debian will <emphasis>never</emphasis> provide propietary (non-free and
undistributable) antivirus software such as: Panda Antivirus,
<!-- 
<ulink
url="http://www.pandasoftware.com/com/linux/linux.asp" name="Panda
Antivirus">,
<ulink
url="http://www.networkassociates.com/us/downloads/evals/"
name="NAI Netshield (uvscan)">, -->
NAI Netshield,
<ulink url="http://www.sophos.com/"
name="Sophos Sweep" />, <ulink url="http://www.antivirus.com"
name="TrendMicro Interscan" />, or <ulink url="http://www.ravantivirus.com"
name="RAV" />. For more pointers see the <ulink
url="http://www.computer-networking.de/~link/security/av-linux_e.txt"
name="Linux antivirus software mini-FAQ" />. This does not mean that
this software cannot be installed properly in a Debian system<footnote><para>
Actually, there is an installer package for the <emphasis>F-prot</emphasis> antivirus,
which is non-free but <emphasis>gratis</emphasis> for home users, called
<command>f-prot-installer</command>. This installer, however, just downloads <ulink
url="http://www.f-prot.com/products/home_use/linux/" name="F-prot's software" />
and installs it in the
system.</para></footnote>.

</para><para>For more information on how to set up a virus detection system
read Dave Jones' article <ulink
url="http://www.linuxjournal.com/article.php?sid=4882" name="Building
an E-mail Virus Detection System for Your Network" />.</para>
</section>


<section id="gpg-agent"><title>GPG agent</title>

<para>It is very common nowadays to digitally sign (and sometimes
encrypt) e-mail. You might, for example, find that many people
participating on mailing lists sign their list e-mail. Public key
signatures are currently the only means to verify that an e-mail was
sent by the sender and not by some other person.

</para><para>Debian GNU/Linux provides a number of e-mail clients with built-in
e-mail signing capabilities that interoperate either with
<application>gnupg</application> or <application>pgp</application>:

<itemizedlist>
<listitem><para><application>evolution</application>.</para></listitem>
<listitem><para><application>mutt</application>.</para></listitem>
<listitem><para><application>kmail</application>.</para></listitem>
<listitem><para><application>icedove</application> (rebranded version of
Mozilla's Thunderbird) through the 
<ulink url="http://enigmail.mozdev.org/" name="Enigmail" /> plugin.
This plugin is provided by the <application>enigmail</application> package.</para></listitem>

<listitem><para><application>sylpheed</application>. Depending on how the stable version
of this package evolves, you may need to use the <emphasis>bleeding edge
version</emphasis>, <application>sylpheed-claws</application>.</para></listitem>

<listitem><para><application>gnus</application>, which when installed with the
<application>mailcrypt</application> package, is an <command>emacs</command>
interface to <command>gnupg</command>.</para></listitem>

<listitem><para><application>kuvert</application>, which provides this functionality
independently of your chosen mail user agent (MUA) by interacting with
the mail transport agent (MTA).</para></listitem>

</itemizedlist>

</para><para>Key servers allow you to download published public keys so that you
may verify signatures. One such key server is <ulink
url="http://wwwkeys.pgp.net" />. <application>gnupg</application> can
automatically fetch public keys that are not already in your public
keyring. For example, to configure <command>gnupg</command> to use the above
key server, edit the file <filename>~/.gnupg/options</filename> and add the
following line:

<footnote><para>
For more examples of how to configure <command>gnupg</command> check
<filename>/usr/share/doc/mutt/examples/gpg.rc</filename>.
</para></footnote>
<screen>
keyserver wwwkeys.pgp.net
</screen>

</para><para>Most key servers are linked, so that when your public key is added
to one server, the addition is propagated to all the other public key
servers. There is also a Debian GNU/Linux package
<application>debian-keyring</application>, that provides all the public keys
of the Debian developers. The <command>gnupg</command> keyrings are
installed in <filename>/usr/share/keyrings/</filename>.

</para><para>For more information:

<itemizedlist>

    <listitem><para><ulink url="http://www.gnupg.org/faq.html" name="GnuPG FAQ" />.</para></listitem>

<listitem><para><ulink url="http://www.gnupg.org/gph/en/manual.html" name="GnuPG
Handbook" />.</para></listitem>

<listitem><para><ulink
url="http://www.dewinter.com/gnupg_howto/english/GPGMiniHowto.html"
name="GnuPG Mini Howto (English)" />.</para></listitem>

<listitem><para><ulink url="http://www.uk.pgp.net/pgpnet/pgp-faq/"
name="comp.security.pgp FAQ" />.</para></listitem>

<listitem><para><ulink url="http://www.cryptnet.net/fdp/crypto/gpg-party.html"
name="Keysigning Party HOWTO" />.</para></listitem>

</itemizedlist></para>
</section>

</chapter>
