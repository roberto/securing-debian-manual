msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2023-08-24 18:19-0400\n"
"PO-Revision-Date: 2023-08-24 18:19-0400\n"
"Last-Translator: Automatically generated\n"
"Language-Team: None\n"
"Language: en-US \n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Publican v4.3.2\n"

msgid "Securing Debian Manual"
msgstr ""

msgid "This document describes security in the Debian project and in the Debian operating system. Starting with the process of securing and hardening the default Debian GNU/Linux distribution installation, it also covers some of the common tasks to set up a secure network environment using Debian GNU/Linux, gives additional information on the security tools available and talks about how security is enforced in Debian by the security and audit team."
msgstr ""

