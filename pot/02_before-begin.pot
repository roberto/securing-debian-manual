msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2023-08-24 18:19-0400\n"
"PO-Revision-Date: 2023-08-24 18:19-0400\n"
"Last-Translator: Automatically generated\n"
"Language-Team: None\n"
"Language: en-US \n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Publican v4.3.2\n"

msgid "Before you begin"
msgstr ""

msgid "What do you want this system for?"
msgstr ""

msgid "Securing Debian is not very different from securing any other system; in order to do it properly, you must first decide what you intend to do with it. After this, you will have to consider that the following tasks need to be taken care of if you want a really secure system."
msgstr ""

msgid "You will find that this manual is written from the bottom up, that is, you will read some information on tasks to do before, during and after you install your Debian system. The tasks can also be thought of as:"
msgstr ""

msgid "Decide which services you need and limit your system to those. This includes deactivating/uninstalling unneeded services, and adding firewall-like filters, or tcpwrappers."
msgstr ""

msgid "Limit users and permissions in your system."
msgstr ""

msgid "Harden offered services so that, in the event of a service compromise, the impact to your system is minimized."
msgstr ""

msgid "Use appropriate tools to guarantee that unauthorized use is detected so that you can take appropriate measures."
msgstr ""

msgid "Be aware of general security problems"
msgstr ""

msgid "The following manual does not (usually) go into the details on why some issues are considered security risks. However, you might want to have a better background regarding general UNIX and (specific) Linux security. Take some time to read over security related documents in order to make informed decisions when you are encountered with different choices. Debian GNU/Linux is based on the Linux kernel, so much of the information regarding Linux, as well as from other distributions and general UNIX security also apply to it (even if the tools used, or the programs available, differ)."
msgstr ""

msgid "Some useful documents include:"
msgstr ""

msgid "The <ulink name=\"Linux Security HOWTO\" url=\"http://www.tldp.org/HOWTO/Security-HOWTO/\" /> (also available at <ulink name=\"LinuxSecurity\" url=\"http://www.linuxsecurity.com/docs/LDP/Security-HOWTO.html\" />) is one of the best references regarding general Linux security."
msgstr ""

msgid "The <ulink name=\"Security Quick-Start HOWTO for Linux\" url=\"http://www.tldp.org/HOWTO/Security-Quickstart-HOWTO/\" /> is also a very good starting point for novice users (both to Linux and security)."
msgstr ""

msgid "The <ulink name=\"Linux Security Administrator's Guide\" url=\"http://seifried.org/lasg/\" /> is a complete guide that touches all the issues related to security in Linux, from kernel security to VPNs. Note that it has not been updated since 2001, but some information is still relevant. <footnote><para> At a given time it was superseded by the \"Linux Security Knowledge Base\". This documentation is also provided in Debian through the <application>lskb</application> package. Now it's back as the <emphasis>Lasg</emphasis> again. </para></footnote>"
msgstr ""

msgid "Kurt Seifried's <ulink name=\"Securing Linux Step by Step\" url=\"http://seifried.org/security/os/linux/20020324-securing-linux-step-by-step.html\" />."
msgstr ""

msgid "In <ulink name=\"Securing and Optimizing Linux: RedHat Edition\" url=\"http://www.tldp.org/links/p_books.html#securing_linux\" /> you can find a similar document to this manual but related to Red Hat, some of the issues are not distribution-specific and also apply to Debian."
msgstr ""

msgid "Another Red Hat related document is <ulink name=\"EAL3 Evaluated Configuration Guide for Red Hat Enterprise\" url=\"http://ltp.sourceforge.net/docs/RHEL-EAL3-Configuration-Guide.pdf\" />."
msgstr ""

msgid "IntersectAlliance has published some documents that can be used as reference cards on how to harden Linux servers (and their services), the documents are available at <ulink name=\"their site\" url=\"http://www.intersectalliance.com/projects/index.html\" />."
msgstr ""

msgid "For network administrators, a good reference for building a secure network is the <ulink name=\"Securing your Domain HOWTO\" url=\"http://www.linuxsecurity.com/docs/LDP/Securing-Domain-HOWTO/\" />."
msgstr ""

msgid "If you want to evaluate the programs you are going to use (or want to build up some new ones) you should read the <ulink name=\"Secure Programs HOWTO\" url=\"http://www.tldp.org/HOWTO/Secure-Programs-HOWTO/\" /> (master copy is available at <ulink url=\"http://www.dwheeler.com/secure-programs/\" />, it includes slides and talks from the author, David Wheeler)"
msgstr ""

msgid "If you are considering installing firewall capabilities, you should read the <ulink name=\"Firewall HOWTO\" url=\"http://www.tldp.org/HOWTO/Firewall-HOWTO.html\" /> and the <ulink name=\"IPCHAINS HOWTO\" url=\"http://www.tldp.org/HOWTO/IPCHAINS-HOWTO.html\" /> (for kernels previous to 2.4)."
msgstr ""

msgid "Finally, a good card to keep handy is the <ulink name=\"Linux Security ReferenceCard\" url=\"http://www.linuxsecurity.com/docs/QuickRefCard.pdf\" />."
msgstr ""

msgid "In any case, there is more information regarding the services explained here (NFS, NIS, SMB...) in many of the HOWTOs of the <ulink name=\"The Linux Documentation Project\" url=\"http://www.tldp.org/\" />. Some of these documents speak on the security side of a given service, so be sure to take a look there too."
msgstr ""

msgid "The HOWTO documents from the Linux Documentation Project are available in Debian GNU/Linux through the installation of the <package>doc-linux-text</package> (text version) or <package>doc-linux-html</package> (HTML version). After installation these documents will be available at the <filename>/usr/share/doc/HOWTO/en-txt</filename> and <filename>/usr/share/doc/HOWTO/en-html</filename> directories, respectively."
msgstr ""

msgid "Other recommended Linux books:"
msgstr ""

msgid "Maximum Linux Security : A Hacker's Guide to Protecting Your Linux Server and Network. Anonymous. Paperback - 829 pages. Sams Publishing. ISBN: 0672313413. July 1999."
msgstr ""

msgid "Linux Security By John S. Flowers. New Riders; ISBN: 0735700354. March 1999."
msgstr ""

msgid "<ulink name=\"Hacking Linux Exposed\" url=\"http://www.linux.org/books/ISBN_0072127732.html\" /> By Brian Hatch. McGraw-Hill Higher Education. ISBN 0072127732. April, 2001"
msgstr ""

msgid "Other books (which might be related to general issues regarding UNIX and security and not Linux specific):"
msgstr ""

msgid "<ulink name=\"Practical Unix and Internet Security (2nd Edition)\" url=\"http://www.ora.com/catalog/puis/noframes.html\" /> Garfinkel, Simpson, and Spafford, Gene; O'Reilly Associates; ISBN 0-56592-148-8; 1004pp; 1996."
msgstr ""

msgid "Firewalls and Internet Security Cheswick, William R. and Bellovin, Steven M.; Addison-Wesley; 1994; ISBN 0-201-63357-4; 320pp."
msgstr ""

msgid "Some useful web sites to keep up to date regarding security:"
msgstr ""

msgid "<ulink name=\"NIST Security Guidelines\" url=\"http://csrc.nist.gov/fasp/index.html\" />."
msgstr ""

msgid "<ulink name=\"Security Focus\" url=\"http://www.securityfocus.com\" /> the server that hosts the Bugtraq vulnerability database and list, and provides general security information, news and reports."
msgstr ""

msgid "<ulink name=\"Linux Security\" url=\"http://www.linuxsecurity.com/\" />. General information regarding Linux security (tools, news...). Most useful is the <ulink name=\"main documentation\" url=\"http://www.linuxsecurity.com/resources/documentation-1.html\" /> page."
msgstr ""

msgid "<ulink name=\"Linux firewall and security site\" url=\" http://www.linux-firewall-tools.com/linux/\" />. General information regarding Linux firewalls and tools to control and administrate them."
msgstr ""

msgid "How does Debian handle security?"
msgstr ""

msgid "Just so you have a general overview of security in Debian GNU/Linux you should take note of the different issues that Debian tackles in order to provide an overall secure system:"
msgstr ""

msgid "Debian problems are always handled openly, even security related. Security issues are discussed openly on the debian-security mailing list. Debian Security Advisories (DSAs) are sent to public mailing lists (both internal and external) and are published on the public server. As the <ulink name=\"Debian Social Contract\" url=\"http://www.debian.org/social_contract\" /> states: <emphasis>We will not hide problems. We will keep our entire bug report database open for public view at all times. Reports that people file online will promptly become visible to others.</emphasis>"
msgstr ""

msgid "Debian follows security issues closely. The security team checks many security related sources, the most important being <ulink name=\"Bugtraq\" url=\"http://www.securityfocus.com/cgi-bin/vulns.pl\" />, on the lookout for packages with security issues that might be included in Debian."
msgstr ""

msgid "Security updates are the first priority. When a security problem arises in a Debian package, the security update is prepared as fast as possible and distributed for our stable, testing and unstable releases, including all architectures."
msgstr ""

msgid "Information regarding security is centralized in a single point, <ulink url=\"http://security.debian.org/\" />."
msgstr ""

msgid "Debian is always trying to improve the overall security of the distribution by starting new projects, such as automatic package signature verification mechanisms."
msgstr ""

msgid "Debian provides a number of useful security related tools for system administration and monitoring. Developers try to tightly integrate these tools with the distribution in order to make them a better suite to enforce local security policies. Tools include: integrity checkers, auditing tools, hardening tools, firewall tools, intrusion detection tools, etc."
msgstr ""

msgid "Package maintainers are aware of security issues. This leads to many \"secure by default\" service installations which could impose certain restrictions on their normal use. Debian does, however, try to balance security and ease of administration - the programs are not de-activated when you install them (as it is the case with say, the BSD family of operating systems). In any case, prominent security issues (such as <literal>setuid</literal> programs) are part of the <ulink name=\"Debian Policy\" url=\"http://www.debian.org/doc/debian-policy/\" />."
msgstr ""

msgid "By publishing security information specific to Debian and complementing other information-security documents related to Debian (see <xref linkend=\"references\" />), this document aims to produce better system installations security-wise."
msgstr ""

