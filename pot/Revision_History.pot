msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2023-08-24 18:19-0400\n"
"PO-Revision-Date: 2023-08-24 18:19-0400\n"
"Last-Translator: Automatically generated\n"
"Language-Team: None\n"
"Language: en-US \n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Publican v4.3.2\n"

msgid "Changelog/History"
msgstr ""

msgid "Marcos"
msgstr ""

msgid "Fouces"
msgstr ""

msgid "Migrate to Docbook XML."
msgstr ""

msgid "Build with Publican. No longer use custom Makefile."
msgstr ""

msgid "Migrate svn repository to git."
msgstr ""

msgid "Import chinese, italian, spanish, portuguese, japanese, russian, french and german translations to PO format."
msgstr ""

msgid "Thijs"
msgstr ""

msgid "Kinkhorst"
msgstr ""

msgid "Clarify FAQ on raw sockets."
msgstr ""

msgid "Update section 4.5 on GRUB2."
msgstr ""

msgid "Replace example postrm user removal code with advice to use deluser/delgroup --system"
msgstr ""

msgid "Remove mention of MD5 shadow passwords."
msgstr ""

msgid "Do not recommend dselect for holding packages."
msgstr ""

msgid "No longer include the Security Team FAQ verbatim, because it duplicates information documented elsewhere and is hence perpetually out of date."
msgstr ""

msgid "Update section on restart after library upgrades to mention needrestart."
msgstr ""

msgid "Avoid gender-specific language. Patch by Myriam."
msgstr ""

msgid "Use LSB headers for firewall script. Patch by Dominic Walden."
msgstr ""

msgid "Javier"
msgstr ""

msgid "Fernández-Sanguino Peña."
msgstr ""

msgid "Indicate that the document is not updated with latest versions."
msgstr ""

msgid "Update pointers to current location of sources."
msgstr ""

msgid "Update information on security updates for newer releases."
msgstr ""

msgid "Point information for Developers to online sources instead of keeping the information in the document, to prevent duplication."
msgstr ""

msgid "Extend the information regarding securing console access, including limiting the Magic SysRq key."
msgstr ""

msgid "Update the information related to PAM modules including how to restrict console logins, use cracklib and use the features avialable in /etc/pam.d/login. Remove the references to obsolete variables in /etc/login.defs."
msgstr ""

msgid "Reference some of the PAM modules available to use double factor authentication, for administrators that want to stop using passwords altogether."
msgstr ""

msgid "Fix shell script example in Appendix."
msgstr ""

msgid "Fix reference errors."
msgstr ""

msgid "Point to the Basille sourceforge project instead of the bastille-unix.org site as it is not responding."
msgstr ""

msgid "Fernández-Sanguino Peña"
msgstr ""

msgid "Change reference to Log Analysis' website as this is no longer available."
msgstr ""

msgid "Change the section related to choosing a filesystem: note that ext3 is now the default."
msgstr ""

msgid "Change the name of the packages related to enigmail to reflect naming changes introduced in Debian."
msgstr ""

msgid "Change URLs pointing to Bastille Linux to www.Bastille-UNIX.org since the domain has been <ulink name=\"purchased by a cybersquatter\" url=\"http://bastille-linux.sourceforge.net/press-release-newname.html\" />."
msgstr ""

msgid "Fix pointers to Linux Ramen and Lion worms."
msgstr ""

msgid "Use linux-image in the examples instead of the (old) kernel-image packages."
msgstr ""

msgid "Fix typos spotted by Francesco Poli."
msgstr ""

msgid "Update the information related to security updates. Drop the text talking about Tiger and include information on the update-notifier and adept tools (for Desktops) as well as debsecan. Also include some pointers to other tools available."
msgstr ""

msgid "Divide the firewall applications based on target users and add fireflier to the Desktop firewall applications list."
msgstr ""

msgid "Remove references to libsafe, it's not in the archive any longer (was removed January 2006)."
msgstr ""

msgid "Fix the location of syslog's configuration, thanks to John Talbut."
msgstr ""

msgid "Thanks go to Francesco Poli for his extensive review of the document."
msgstr ""

msgid "Remove most references to the woody release as it is no longer available (in the archive) and security support for it is no longer available."
msgstr ""

msgid "Describe how to restrict users so that they can only do file transfers."
msgstr ""

msgid "Added a note regarding the debian-private declasiffication decision."
msgstr ""

msgid "Updated link of incident handling guides."
msgstr ""

msgid "Added a note saying that development tools (compilers, etc.) are not installed now in the default 'etch' installation."
msgstr ""

msgid "Fix references to the master security server."
msgstr ""

msgid "Add pointers to additional APT-secure documentation."
msgstr ""

msgid "Improve the description of APT signatures."
msgstr ""

msgid "Comment out some things which are not yet final related to the mirror's official public keys."
msgstr ""

msgid "Fixed name of the Debian Testing Security Team."
msgstr ""

msgid "Remove reference to sarge in an example."
msgstr ""

msgid "Update the antivirus section, clamav is now available on the release. Also mention the f-prot installer."
msgstr ""

msgid "Removes all references to freeswan as it is obsolete."
msgstr ""

msgid "Describe issues related to ruleset changes to the firewall if done remotely and provide some tips (in footnotes)."
msgstr ""

msgid "Update the information related to the IDS installation, mention BASE and the need to setup a logging database."
msgstr ""

msgid "Rewrite the \"running bind as a non-root user\" section as this no longer applies to Bind9. Also remove the reference to the init.d script since the changes need to be done through /etc/default."
msgstr ""

msgid "Remove the obsolete way to setup iptables rulesets as woody is no longer supported."
msgstr ""

msgid "Revert the advice regarding LOG_UNKFAIL_ENAB it should be set to 'no' (as per default)."
msgstr ""

msgid "Added more information related to updating the system with desktop tools (including update-notifier) and describe aptitude usage to update the system. Also note that dselect is deprecated."
msgstr ""

msgid "Updated the contents of the FAQ and remove redundant paragraphs."
msgstr ""

msgid "Review and update the section related to forensic analysis of malware."
msgstr ""

msgid "Remove or fix some dead links."
msgstr ""

msgid "Fix many typos and gramatical errors reported by Francesco Poli."
msgstr ""

msgid "Provide examples using apt-cache's rdepends as suggested by Ozer Sarilar."
msgstr ""

msgid "Fix location of Squid's user's manual because of its relocation as notified by Oskar Pearson (its maintainer)."
msgstr ""

msgid "Fix information regarding umask, it's logins.defs (and not limits.conf) where this can be configured for all login connections. Also state what is Debian's default and what would be a more restrictive value for both users and root. Thanks to Reinhard Tartler for spotting the bug."
msgstr ""

msgid "Add information on how to track security vulnerabilities and add references to the Debian Testing Security Tracker."
msgstr ""

msgid "Add more information on the security support for testing."
msgstr ""

msgid "Fix a large number of typos with a patch provided by Simon Brandmair."
msgstr ""

msgid "Added section on how to disable root prompt on initramfs provided by Max Attems."
msgstr ""

msgid "Remove references to queso."
msgstr ""

msgid "Note that testing is now security-supported in the introduction."
msgstr ""

msgid "Rewrote the information on how to setup ssh chroots to clarify the different options available, thank to Bruce Park for bringing up the different mistakes in this appendix."
msgstr ""

msgid "Fix lsof call as suggested by Christophe Sahut."
msgstr ""

msgid "Include patches for typo fixes from Uwe Hermann."
msgstr ""

msgid "Fix typo in reference spotted by Moritz Naumann."
msgstr ""

msgid "Add a section on Debian Developer's best practices for security."
msgstr ""

msgid "Ammended firewall script with comments from WhiteGhost."
msgstr ""

msgid "Included a patch from Thomas Sjögren which describes that <literal>noexec</literal> works as expected with \"new\" kernels, adds information regarding tempfile handling, and some new pointers to external documentation."
msgstr ""

msgid "Add a pointer to Dan Farmer's and Wietse Venema's forensic discovery web site, as suggested by Freek Dijkstra, and expanded a little bit the forensic analysis section with more pointers."
msgstr ""

msgid "Fixed URL of Italy's CERT, thanks to Christoph Auer."
msgstr ""

msgid "Reuse Joey Hess' information at the wiki on secure apt and introduce it in the infrastructure section."
msgstr ""

msgid "Review sections referring to old versions (woody or potato)."
msgstr ""

msgid "Fix some cosmetic issues with patch from Simon Brandmair."
msgstr ""

msgid "Included patches from Carlo Perassi: acl patches are obsolete, openwall patches are obsolete too, removed fixme notes about 2.2 and 2.4 series kernels, hap is obsolete (and not present in WNPP), remove references to Immunix (StackGuard is now in Novell's hands), and fix a FIXME about the use of bsign or elfsign."
msgstr ""

msgid "Updated references to SElinux web pages to point to the Wiki (currently the most up to date source of information)."
msgstr ""

msgid "Include file tags and make a more consistent use of \"MD5 sum\" with a patch from Jens Seidel."
msgstr ""

msgid "Patch from Joost van Baal improving the information on the firewall section (pointing to the wiki instead of listing all firewall packages available) (Closes: #339865)."
msgstr ""

msgid "Review the FAQ section on vulnerability stats, thanks to Carlos Galisteo de Cabo for pointing out that it was out of date."
msgstr ""

msgid "Use the quote from the Social Contract 1.1 instead of 1.0 as suggested by Francesco Poli."
msgstr ""

msgid "Note on the SSH section that the chroot will not work if using the nodev option in the partition and point to the latest ssh packages with the chroot patch, thanks to Lutz Broedel for pointing these issues out."
msgstr ""

msgid "Fix typo spotted by Marcos Roberto Greiner (md5sum should be sha1sum in code snippet)."
msgstr ""

msgid "Included Jens Seidel's patch fixing a number of package names and typos."
msgstr ""

msgid "Slightly update of the tools section, removed tools no longer available and added some new ones."
msgstr ""

msgid "Rewrite parts of the section related to where to find this document and what formats are available (the website does provide a PDF version). Also note that copies on other sites and translations might be obsolete (many of the Google hits for the manual in other sites are actually out of date)."
msgstr ""

msgid "Improved the after installation security enhancements related to kernel configuration for network level protection with a sysctl.conf file provided by Will Moy."
msgstr ""

msgid "Improved the gdm section, thanks to Simon Brandmair."
msgstr ""

msgid "Typo fixes from Frédéric Bothamy and Simon Brandmair."
msgstr ""

msgid "Improvements in the after installation sections related to how to generate the MD5 (or SHA-1) sums of binaries for periodic review."
msgstr ""

msgid "Updated the after installation sections regarding checksecurity configuration (was out of date)."
msgstr ""

msgid "Added a code snippet to use grep-available to generate the list of packages depending on Perl. As requested in #302470."
msgstr ""

msgid "Rewrite of the section on network services (which ones are installed and how to disable them)."
msgstr ""

msgid "Added more information to the honeypot deployment section mentioning useful Debian packages."
msgstr ""

msgid "Expanded the PAM configuration limits section."
msgstr ""

msgid "Added information on how to use pam_chroot for openssh (based on pam_chroot's README)."
msgstr ""

msgid "Fixed some minor issues reported by Dan Jacobson."
msgstr ""

msgid "Updated the kernel patches information partially based on a patch from Carlo Perassi and also by adding deprecation notes and new kernel patches available (adamantix)."
msgstr ""

msgid "Included patch from Simon Brandmair that fixes a sentence related to login failures in terminal."
msgstr ""

msgid "Added Mozilla/Thunderbird to the valid GPG agents as suggested by Kapolnai Richard."
msgstr ""

msgid "Expanded the section on security updates mentioning library and kernel updates and how to detect when services need to be restarted."
msgstr ""

msgid "Rewrote the firewall section, moved the information that applies to woody down and expand the other sections including some information on how to manually set the firewall (with a sample script) and how to test the firewall configuration."
msgstr ""

msgid "Added some information preparing for the 3.1 release."
msgstr ""

msgid "Added more detailed information on kernel upgrades, specifically targeted at those that used the old installation system."
msgstr ""

msgid "Added a small section on the experimental apt 0.6 release which provides package signing checks. Moved old content to the section and also added a pointer to changes made in aptitude."
msgstr ""

msgid "Typo fixes spotted by Frédéric Bothamy."
msgstr ""

msgid "Added clarification to ro /usr with patch from Joost van Baal."
msgstr ""

msgid "Apply patch from Jens Seidel fixing many typos."
msgstr ""

msgid "FreeSWAN is dead, long live OpenSWAN."
msgstr ""

msgid "Added information on restricting access to RPC services (when they cannot be disabled) also included patch provided by Aarre Laakso."
msgstr ""

msgid "Update aj's apt-check-sigs script."
msgstr ""

msgid "Apply patch Carlo Perassi fixing URLs."
msgstr ""

msgid "Apply patch from Davor Ocelic fixing many errors, typos, urls, grammar and FIXMEs. Also adds some additional information to some sections."
msgstr ""

msgid "Rewrote the section on user auditing, highlight the usage of script which does not have some of the issues associated to shell history."
msgstr ""

msgid "Rewrote the user-auditing information and include examples on how to use script."
msgstr ""

msgid "Added information on references in DSAs and CVE-Compatibility."
msgstr ""

msgid "Added information on apt 0.6 (apt-secure merge in experimental)."
msgstr ""

msgid "Fixed location of Chroot daemons HOWTO as suggested by Shuying Wang."
msgstr ""

msgid "Changed APACHECTL line in the Apache chroot example (even if its not used at all) as suggested by Leonard Norrgard."
msgstr ""

msgid "Added a footnote regarding hardlink attacks if partitions are not setup properly."
msgstr ""

msgid "Added some missing steps in order to run bind as named as provided by Jeffrey Prosa."
msgstr ""

msgid "Added notes about Nessus and Snort out-of-dateness in woody and availability of backported packages."
msgstr ""

msgid "Added a chapter regarding periodic integrity test checks."
msgstr ""

msgid "Clarified the status of testing regarding security updates (Debian bug 233955)."
msgstr ""

msgid "Added more information regarding expected contents in securetty (since it's kernel specific)."
msgstr ""

msgid "Added pointer to snoopylogger (Debian bug 179409)."
msgstr ""

msgid "Added reference to guarddog (Debian bug 170710)."
msgstr ""

msgid "<command>apt-ftparchive</command> is in <package>apt-utils</package>, not in <package>apt</package> (thanks to Emmanuel Chantreau for pointing this out)."
msgstr ""

msgid "Removed jvirus from AV list."
msgstr ""

msgid "Fixed URL as suggested by Frank Lichtenheld."
msgstr ""

msgid "Fixed PermitRootLogin typo as suggested by Stefan Lindenau."
msgstr ""

msgid "Added those that have made the most significant contributions to this manual (please mail me if you think you should be in the list and are not)."
msgstr ""

msgid "Added some blurb about FIXME/TODOs."
msgstr ""

msgid "Moved the information on security updates to the beginning of the section as suggested by Elliott Mitchell."
msgstr ""

msgid "Added grsecurity to the list of kernel-patches for security but added a footnote on the current issues with it as suggested by Elliott Mitchell."
msgstr ""

msgid "Removed loops (echo to 'all') in the kernel's network security script as suggested by Elliott Mitchell."
msgstr ""

msgid "Added more (up-to-date) information in the antivirus section."
msgstr ""

msgid "Rewrote the buffer overflow protection section and added more information on patches to the compiler to enable this kind of protection."
msgstr ""

msgid "Removed (and then re-added) appendix on chrooting Apache. The appendix is now dual-licensed."
msgstr ""

msgid "Fixed typos spotted by Leonard Norrgard."
msgstr ""

msgid "Added a section on how to contact CERT for incident handling (<xref linkend=\"after-compromise\" />)."
msgstr ""

msgid "More information on setting up a Squid proxy."
msgstr ""

msgid "Added a pointer and removed a FIXME thanks to Helge H. F."
msgstr ""

msgid "Fixed a typo (save_inactive) spotted by Philippe Faes."
msgstr ""

msgid "Fixed several typos spotted by Jaime Robles."
msgstr ""

msgid "Following Maciej Stachura's suggestions I've expanded the section on limiting users."
msgstr ""

msgid "Fixed typo spotted by Wolfgang Nolte."
msgstr ""

msgid "Fixed links with patch contributed by Ruben Leote Mendes"
msgstr ""

msgid "Added a link to David Wheeler's excellent document on the footnote about counting security vulnerabilities."
msgstr ""

msgid "Frédéric"
msgstr ""

msgid "Schütz"
msgstr ""

msgid "rewrote entirely the section of ext2 attributes (lsattr/chattr)"
msgstr ""

msgid "Merge section 9.3 (\"useful kernel patches\") into section 4.13 (\"Adding kernel patches\"), and added some content."
msgstr ""

msgid "Added a few more TODOs."
msgstr ""

msgid "Added information on how to manually check for updates and also about cron-apt. That way Tiger is not perceived as the only way to do automatic update checks."
msgstr ""

msgid "Slightly rewrite of the section on executing a security updates due to Jean-Marc Ranger comments."
msgstr ""

msgid "Added a note on Debian's installation (which will suggest the user to execute a security update right after installation)."
msgstr ""

msgid "Added a patch contributed by Frédéric Schütz."
msgstr ""

msgid "Added a few more references on capabilities thanks to Frédéric."
msgstr ""

msgid "Slight changes in the bind section adding a reference to BIND's 9 online documentation and proper references in the first area (Hi Pedro!)."
msgstr ""

msgid "Fixed the changelog date - new year :-)."
msgstr ""

msgid "Added a reference to Colin's articles for the TODOs."
msgstr ""

msgid "Removed reference to old ssh+chroot patches."
msgstr ""

msgid "More patches from Carlo Perassi."
msgstr ""

msgid "Typo fixes (recursive in Bind is recursion), pointed out by Maik Holtkamp."
msgstr ""

msgid "Reorganized the information on chroot (merged two sections, it didn't make much sense to have them separated)."
msgstr ""

msgid "Added the notes on chrooting Apache provided by Alexandre Ratti."
msgstr ""

msgid "Applied patches contributed by Guillermo Jover."
msgstr ""

msgid "Applied patches from Carlo Perassi, fixes include: re-wrapping the lines, URL fixes, and fixed some FIXMEs."
msgstr ""

msgid "Updated the contents of the Debian security team FAQ."
msgstr ""

msgid "Added a link to the Debian security team FAQ and the Debian Developer's reference, the duplicated sections might (just might) be removed in the future."
msgstr ""

msgid "Fixed the hand-made auditing section with comments from Michal Zielinski."
msgstr ""

msgid "Added links to wordlists (contributed by Carlo Perassi)."
msgstr ""

msgid "Fixed some typos (still many around)."
msgstr ""

msgid "Fixed TDP links as suggested by John Summerfield."
msgstr ""

msgid "Some typo fixes contributed by Tuyen Dinh, Bartek Golenko and Daniel K. Gebhart."
msgstr ""

msgid "Note regarding /dev/kmem rootkits contributed by Laurent Bonnaud."
msgstr ""

msgid "Fixed typos and FIXMEs contributed by Carlo Perassi."
msgstr ""

msgid "Cris"
msgstr ""

msgid "Tillman"
msgstr ""

msgid "Changed around to improve grammar/spelling."
msgstr ""

msgid "s/host.deny/hosts.deny/ (1 place)."
msgstr ""

msgid "Applied Larry Holish's patch (quite big, fixes a lot of FIXMEs)."
msgstr ""

msgid "Fixed minor typos submitted by Thiemo Nagel."
msgstr ""

msgid "Added a footnote suggested by Thiemo Nagel."
msgstr ""

msgid "Fixed an URL link."
msgstr ""

msgid "Applied a patch contributed by Philipe Gaspar regarding the Squid which also kills a FIXME."
msgstr ""

msgid "Yet another FAQ item regarding service banners taken from the debian-security mailing list (thread \"Telnet information\" started 26th July 2002)."
msgstr ""

msgid "Added a note regarding use of CVE cross references in the <emphasis>How much time does the Debian security team...</emphasis> FAQ item."
msgstr ""

msgid "Added a new section regarding ARP attacks contributed by Arnaud \"Arhuman\" Assad."
msgstr ""

msgid "New FAQ item regarding dmesg and console login by the kernel."
msgstr ""

msgid "Small tidbits of information to the signature-checking issues in packages (it seems to not have gotten past beta release)."
msgstr ""

msgid "New FAQ item regarding vulnerability assessment tools false positives."
msgstr ""

msgid "Added new sections to the chapter that contains information on package signatures and reorganized it as a new <emphasis>Debian Security Infrastructure</emphasis> chapter."
msgstr ""

msgid "New FAQ item regarding Debian vs. other Linux distributions."
msgstr ""

msgid "New section on mail user agents with GPG/PGP functionality in the security tools chapter."
msgstr ""

msgid "Clarified how to enable MD5 passwords in woody, added a pointer to PAM as well as a note regarding the max definition in PAM."
msgstr ""

msgid "Added a new appendix on how to create chroot environments (after fiddling a bit with makejail and fixing, as well, some of its bugs), integrated duplicate information in all the appendix."
msgstr ""

msgid "Added some more information regarding <application>SSH</application> chrooting and its impact on secure file transfers. Some information has been retrieved from the debian-security mailing list (June 2002 thread: <emphasis>secure file transfers</emphasis>)."
msgstr ""

msgid "New sections on how to do automatic updates on Debian systems as well as the caveats of using testing or unstable regarding security updates."
msgstr ""

msgid "New section regarding keeping up to date with security patches in the <emphasis>Before compromise</emphasis> section as well as a new section about the debian-security-announce mailing list."
msgstr ""

msgid "Added information on how to automatically generate strong passwords."
msgstr ""

msgid "New section regarding login of idle users."
msgstr ""

msgid "Reorganized the securing mail server section based on the <emphasis>Secure/hardened/minimal Debian (or \"Why is the base system the way it is?\")</emphasis> thread on the debian-security mailing list (May 2002)."
msgstr ""

msgid "Reorganized the section on kernel network parameters, with information provided in the debian-security mailing list (May 2002, <emphasis>syn flood attacked?</emphasis> thread) and added a new FAQ item as well."
msgstr ""

msgid "New section on how to check users passwords and which packages to install for this."
msgstr ""

msgid "New section on PPTP encryption with Microsoft clients discussed in the debian-security mailing list (April 2002)."
msgstr ""

msgid "Added a new section describing what problems are there when binding any given service to a specific IP address, this information was written based on the Bugtraq mailing list in the thread: <emphasis>Linux kernel 2.4 \"weak end host\" issue (previously discussed on debian-security as \"arp problem\")</emphasis> (started on May 9th 2002 by Felix von Leitner)."
msgstr ""

msgid "Added information on <application>ssh</application> protocol version 2."
msgstr ""

msgid "Added two subsections related to Apache secure configuration (the things specific to Debian, that is)."
msgstr ""

msgid "Added a new FAQ related to raw sockets, one related to /root, an item related to users' groups and another one related to log and configuration files permissions."
msgstr ""

msgid "Added a pointer to a bug in libpam-cracklib that might still be open... (need to check)."
msgstr ""

msgid "Added more information regarding forensics analysis (pending more information on packet inspection tools such as <application>tcpflow</application>)."
msgstr ""

msgid "Changed the \"what should I do regarding compromise\" into a bullet list and included some more stuff."
msgstr ""

msgid "Added some information on how to set up the Xscreensaver to lock the screen automatically after the configured timeout."
msgstr ""

msgid "Added a note related to the utilities you should not install in the system. Included a note regarding Perl and why it cannot be easily removed in Debian. The idea came after reading Intersect's documents regarding Linux hardening."
msgstr ""

msgid "Added information on lvm and journalling file systems, ext3 recommended. The information there might be too generic, however."
msgstr ""

msgid "Added a link to the online text version (check)."
msgstr ""

msgid "Added some more stuff to the information on firewalling the local system, triggered by a comment made by Hubert Chan in the mailing list."
msgstr ""

msgid "Added more information on PAM limits and pointers to Kurt Seifried's documents (related to a post by him to Bugtraq on April 4th 2002 answering a person that had ``discovered'' a vulnerability in Debian GNU/Linux related to resource starvation)."
msgstr ""

msgid "As suggested by Julián Muñoz, provided more information on the default Debian umask and what a user can access if given a shell in the system (scary, huh?)."
msgstr ""

msgid "Included a note in the BIOS password section due to a comment from Andreas Wohlfeld."
msgstr ""

msgid "Included patches provided by Alfred E. Heggestad fixing many of the typos still present in the document."
msgstr ""

msgid "Added a pointer to the changelog in the Credits section since most people who contribute are listed here (and not there)."
msgstr ""

msgid "Added a few more notes to the chattr section and a new section after installation talking about system snapshots. Both ideas were contributed by Kurt Pomeroy."
msgstr ""

msgid "Added a new section after installation just to remind users to change the boot-up sequence."
msgstr ""

msgid "Added some more TODO items provided by Korn Andras."
msgstr ""

msgid "Added a pointer to the NIST's guidelines on how to secure DNS provided by Daniel Quinlan."
msgstr ""

msgid "Added a small paragraph regarding Debian's SSL certificates infrastructure."
msgstr ""

msgid "Added Daniel Quinlan's suggestions regarding <application>ssh</application> authentication and exim's relay configuration."
msgstr ""

msgid "Added more information regarding securing bind including changes suggested by Daniel Quinlan and an appendix with a script to make some of the changes commented on in that section."
msgstr ""

msgid "Added a pointer to another item regarding Bind chrooting (needs to be merged)."
msgstr ""

msgid "Added a one liner contributed by Cristian Ionescu-Idbohrn to retrieve packages with tcpwrappers support."
msgstr ""

msgid "Added a little bit more info on Debian's default PAM setup."
msgstr ""

msgid "Included a FAQ question about using PAM to provide services without shell accounts."
msgstr ""

msgid "Moved two FAQ items to another section and added a new FAQ regarding attack detection (and compromised systems)."
msgstr ""

msgid "Included information on how to set up a bridge firewall (including a sample Appendix). Thanks to Francois Bayart who sent this to me in March."
msgstr ""

msgid "Added a FAQ regarding the syslogd's <emphasis>MARK heartbeat</emphasis> from a question answered by Noah Meyerhans and Alain Tesio in December 2001."
msgstr ""

msgid "Included information on buffer overflow protection as well as some information on kernel patches."
msgstr ""

msgid "Added more information (and reorganized) the firewall section. Updated the information regarding the iptables package and the firewall generators available."
msgstr ""

msgid "Reorganized the information regarding log checking, moved logcheck information from host intrusion detection to that section."
msgstr ""

msgid "Added some information on how to prepare a static package for bind for chrooting (untested)."
msgstr ""

msgid "Added a FAQ item regarding some specific servers/services (could be expanded with some of the recommendations from the debian-security list)."
msgstr ""

msgid "Added some information on RPC services (and when it's necessary)."
msgstr ""

msgid "Added some more information on capabilities (and what lcap does). Is there any good documentation on this? I haven't found any documentation on my 2.4 kernel."
msgstr ""

msgid "Fixed some typos."
msgstr ""

msgid "Rewritten part of the BIOS section."
msgstr ""

msgid "Wrapped most file locations with the file tag."
msgstr ""

msgid "Fixed typo noticed by Edi Stojicevi."
msgstr ""

msgid "Slightly changed the remote audit tools section."
msgstr ""

msgid "Added some todo items."
msgstr ""

msgid "Added more information regarding printers and cups config file (taken from a thread on debian-security)."
msgstr ""

msgid "Added a patch submitted by Jesus Climent regarding access of valid system users to Proftpd when configured as anonymous server."
msgstr ""

msgid "Small change on partition schemes for the special case of mail servers."
msgstr ""

msgid "Added Hacking Linux Exposed to the books section."
msgstr ""

msgid "Fixed directory typo noticed by Eduardo Pérez Ureta."
msgstr ""

msgid "Fixed /etc/ssh typo in checklist noticed by Edi Stojicevi."
msgstr ""

msgid "Fixed location of dpkg conffile."
msgstr ""

msgid "Remove Alexander from contact information."
msgstr ""

msgid "Added alternate mail address."
msgstr ""

msgid "Fixed Alexander mail address (even if commented out)."
msgstr ""

msgid "Fixed location of release keys (thanks to Pedro Zorzenon for pointing this out)."
msgstr ""

msgid "Fixed typos, thanks to Jamin W. Collins."
msgstr ""

msgid "Added a reference to apt-extracttemplate manpage (documents the APT::ExtractTemplate config)."
msgstr ""

msgid "Added section about restricted SSH. Information based on that posted by Mark Janssen, Christian G. Warden and Emmanuel Lacour on the debian-security mailing list."
msgstr ""

msgid "Added information on antivirus software."
msgstr ""

msgid "Added a FAQ: su logs due to the cron running as root."
msgstr ""

msgid "Changed FIXME from lshell thanks to Oohara Yuuma."
msgstr ""

msgid "Added package to sXid and removed comment since it *is* available."
msgstr ""

msgid "Fixed a number of typos discovered by Oohara Yuuma."
msgstr ""

msgid "ACID is now available in Debian (in the acidlab package) thanks to Oohara Yuuma for noticing."
msgstr ""

msgid "Fixed LinuxSecurity links (thanks to Dave Wreski for telling)."
msgstr ""

msgid "Converted the HOWTO into a Manual (now I can properly say RTFM)."
msgstr ""

msgid "Added more information regarding tcp wrappers and Debian (now many services are compiled with support for them so it's no longer an <application>inetd</application> issue)."
msgstr ""

msgid "Clarified the information on disabling services to make it more consistent (rpc info still referred to update-rc.d)."
msgstr ""

msgid "Added small note on lprng."
msgstr ""

msgid "Added some more info on compromised servers (still very rough)."
msgstr ""

msgid "Fixed typos reported by Mark Bucciarelli."
msgstr ""

msgid "Added some more steps in password recovery to cover the cases when the admin has set paranoid-mode=on."
msgstr ""

msgid "Added some information to set paranoid-mode=on when login in console."
msgstr ""

msgid "New paragraph to introduce service configuration."
msgstr ""

msgid "Reorganized the <emphasis>After installation</emphasis> section so it is more broken up into several issues and it's easier to read."
msgstr ""

msgid "Wrote information on how to set up firewalls with the standard Debian 3.0 setup (iptables package)."
msgstr ""

msgid "Small paragraph explaining why installing connected to the Internet is not a good idea and how to avoid this using Debian tools."
msgstr ""

msgid "Small paragraph on timely patching referencing to IEEE paper."
msgstr ""

msgid "Appendix on how to set up a Debian snort box, based on what Vladimir sent to the debian-security mailing list (September 3rd 2001)."
msgstr ""

msgid "Information on how logcheck is set up in Debian and how it can be used to set up HIDS."
msgstr ""

msgid "Information on user accounting and profile analysis."
msgstr ""

msgid "Included apt.conf configuration for read-only /usr copied from Olaf Meeuwissen's post to the debian-security mailing list."
msgstr ""

msgid "New section on VPN with some pointers and the packages available in Debian (needs content on how to set up the VPNs and Debian-specific issues), based on Jaroslaw Tabor's and Samuli Suonpaa's post to debian-security."
msgstr ""

msgid "Small note regarding some programs to automatically build chroot jails."
msgstr ""

msgid "New FAQ item regarding identd based on a discussion in the debian-security mailing list (February 2002, started by Johannes Weiss)."
msgstr ""

msgid "New FAQ item regarding <application>inetd</application> based on a discussion in the debian-security mailing list (February 2002)."
msgstr ""

msgid "Introduced note on rcconf in the \"disabling services\" section."
msgstr ""

msgid "Varied the approach regarding LKM, thanks to Philipe Gaspar."
msgstr ""

msgid "Added pointers to CERT documents and Counterpane resources."
msgstr ""

msgid "Added a new FAQ item regarding time to fix security vulnerabilities."
msgstr ""

msgid "Reorganized FAQ sections."
msgstr ""

msgid "Started writing a section regarding firewalling in Debian GNU/Linux (could be broadened a bit)."
msgstr ""

msgid "Fixed typos sent by Matt Kraai."
msgstr ""

msgid "Fixed DNS information."
msgstr ""

msgid "Added information on whisker and nbtscan to the auditing section."
msgstr ""

msgid "Fixed some wrong URLs."
msgstr ""

msgid "Added a new section regarding auditing using Debian GNU/Linux."
msgstr ""

msgid "Added info regarding finger daemon taken from the security mailing list."
msgstr ""

msgid "Fixed link for Linux Trustees."
msgstr ""

msgid "Fixed typos (patches from Oohara Yuuma and Pedro Zorzenon)."
msgstr ""

msgid "Reorganized service installation and removal and added some new notes."
msgstr ""

msgid "Added some notes regarding using integrity checkers as intrusion detection tools."
msgstr ""

msgid "Added a chapter regarding package signatures."
msgstr ""

msgid "Added notes regarding Squid security sent by Philipe Gaspar."
msgstr ""

msgid "Fixed rootkit links thanks to Philipe Gaspar."
msgstr ""

msgid "Added some notes regarding Apache and Lpr/lpng."
msgstr ""

msgid "Added some information regarding noexec and read-only partitions."
msgstr ""

msgid "Rewrote how users can help in Debian security issues (FAQ item)."
msgstr ""

msgid "Fixed location of mail program."
msgstr ""

msgid "Added some new items to the FAQ."
msgstr ""

msgid "Added a small section on how Debian handles security."
msgstr ""

msgid "Clarified MD5 passwords (thanks to `rocky')."
msgstr ""

msgid "Added some more information regarding harden-X from Stephen van Egmond."
msgstr ""

msgid "Added some forensics information sent by Yotam Rubin."
msgstr ""

msgid "Added information on how to build a honeynet using Debian GNU/Linux."
msgstr ""

msgid "Added some more TODOS."
msgstr ""

msgid "Fixed more typos (thanks Yotam!)."
msgstr ""

msgid "Added patch to fix misspellings and some new information (contributed by Yotam Rubin)."
msgstr ""

msgid "Added references to other online (and offline) documentation both in a section (see <xref linkend=\"securityreferences\" />) by itself and inline in some sections."
msgstr ""

msgid "Added some information on configuring Bind options to restrict access to the DNS server."
msgstr ""

msgid "Added information on how to automatically harden a Debian system (regarding the harden package and bastille)."
msgstr ""

msgid "Removed some done TODOs and added some new ones."
msgstr ""

msgid "Added the default user/group list provided by Joey Hess to the debian-security mailing list."
msgstr ""

msgid "Added information on LKM root-kits (<xref linkend=\"LKM\" />) contributed by Philipe Gaspar."
msgstr ""

msgid "Added information on Proftp contributed by Emmanuel Lacour."
msgstr ""

msgid "Recovered the checklist Appendix from Era Eriksson."
msgstr ""

msgid "Added some new TODO items and removed other fixed ones."
msgstr ""

msgid "Manually included Era's patches since they were not all included in the previous version."
msgstr ""

msgid "Era"
msgstr ""

msgid "Eriksson"
msgstr ""

msgid "Typo fixes and wording changes."
msgstr ""

msgid "Minor changes to tags in order to keep on removing the tt tags and substitute prgn/package tags for them."
msgstr ""

msgid "Added pointer to document as published in the DDP (should supersede the original in the near future)."
msgstr ""

msgid "Started a mini-FAQ (should be expanded) with some questions recovered from my mailbox."
msgstr ""

msgid "Added general information to consider while securing."
msgstr ""

msgid "Added a paragraph regarding local (incoming) mail delivery."
msgstr ""

msgid "Added some pointers to more information."
msgstr ""

msgid "Added information regarding the printing service."
msgstr ""

msgid "Added a security hardening checklist."
msgstr ""

msgid "Reorganized NIS and RPC information."
msgstr ""

msgid "Added some notes taken while reading this document on my new Visor :)."
msgstr ""

msgid "Fixed some badly formatted lines."
msgstr ""

msgid "Added a Genius/Paranoia idea contributed by Gaby Schilders."
msgstr ""

msgid "Josip"
msgstr ""

msgid "Rodin"
msgstr ""

msgid "Added paragraphs related to BIND and some FIXMEs."
msgstr ""

msgid "Small setuid check paragraph"
msgstr ""

msgid "Various minor cleanups."
msgstr ""

msgid "Found out how to use <literal>sgml2txt -f</literal> for the txt version."
msgstr ""

msgid "Added a security update after installation paragraph."
msgstr ""

msgid "Added a proftpd paragraph."
msgstr ""

msgid "This time really wrote something about XDM, sorry for last time."
msgstr ""

msgid "Lots of grammar corrections by James Treacy, new XDM paragraph."
msgstr ""

msgid "Typo fixes, miscellaneous additions."
msgstr ""

msgid "Initial release."
msgstr ""

